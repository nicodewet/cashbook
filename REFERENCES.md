# Kotlin and Other Technical References

* [Kotlin and JUnit 5 @BeforeAll](https://dzone.com/articles/kotlin-and-junit-5-beforeall)
* [JUnit 5 for Kotlin Developers](https://www.baeldung.com/junit-5-kotlin)
* [Java Bean Validation Basics - (JSR 380 / Bean Validation 2.0)](https://www.baeldung.com/javax-validation)
* [javax.money](http://javamoney.github.io/api.html) - see associated [jackson-datatype-money](https://docs.spring.io/spring/docs/5.0.8.RELEASE/spring-framework-reference/web.html#mvc-config-message-converters)
* [Spring MVC @ControllerAdvice](https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/web/bind/annotation/ControllerAdvice.html)
* [Spring MVC REST Error Handling With @ControllerAdvice](https://www.petrikainulainen.net/programming/spring-framework/spring-from-the-trenches-adding-validation-to-a-rest-api/)
* [Spring MVC Exceptions (@ControllerAdvice) - Official Documentation](https://docs.spring.io/spring/docs/5.0.8.RELEASE/spring-framework-reference/web.html#mvc-ann-exceptionhandler)
* [Exception Handling in Spring MVC](https://spring.io/blog/2013/11/01/exception-handling-in-spring-mvc)
* [Kotlin's a great language for JSON](https://medium.com/square-corner-blog/kotlins-a-great-language-for-json-fcd6ef99256b)
* [Guide to Spring Boot REST API Error Handling](https://www.toptal.com/java/spring-boot-rest-api-error-handling)
* [Building Java Docker images with Gradle and Docker multistage builds](http://paulbakker.io/java/docker-gradle-multistage/)

## Pure Kotlin

* [Where Should I Keep My Constants in Kotlin?](https://blog.egorand.me/where-do-i-put-my-constants-in-kotlin/)
* [Exploring Kotlin’s hidden costs — Part 1](https://medium.com/@BladeCoder/exploring-kotlins-hidden-costs-part-1-fbb9935d9b62)
* [Kotlin Basics: apply() and copy()](https://preslav.me/2016/06/26/kotlin-basics-apply-and-copy/)
* [Kotlin Fun With The Factory Pattern](https://medium.com/the-coding-matrix/kotlin-fun-with-the-factory-pattern-70aa5e431dba)

## Testing

* [JsonPath](https://github.com/json-path/JsonPath)
* [Hamcrest](https://code.google.com/archive/p/hamcrest/wikis/Tutorial.wiki)

## Code Coverage

* [Test coverage in Kotlin with Jacoco](https://kevcodez.de/index.php/2018/08/test-coverage-in-kotlin-with-jacoco/)
* [The JaCoCo Plugin](https://docs.gradle.org/current/userguide/jacoco_plugin.html)
* [The JaCoCo Plugin: Enforcing code coverage metrics](https://docs.gradle.org/current/userguide/jacoco_plugin.html#sec:jacoco_report_violation_rules)

See [this thread](https://community.sonarsource.com/t/sonarkotlin-code-coverage/1269) when it comes to Kotlin and 
SonarQube / SonarCloud.io

* [SonarCloud installed plugins list](https://sonarcloud.io/api/plugins/installed)

## REST

* [Richardson Maturity Model - steps toward the glory of REST](https://martinfowler.com/articles/richardsonMaturityModel.html)
* [REST API Design](https://chase-seibert.github.io/blog/2015/04/24/rest-api-design.html)
* [REST API Design - Resource Modelling](https://www.thoughtworks.com/insights/blog/rest-api-design-resource-modeling)

## JPA

* [Working with Kotlin and JPA](https://www.baeldung.com/kotlin-jpa)
* [Kotlin JPA OneToMany Example](https://stackoverflow.com/a/49580219)
* [Kotlin and Spring: Working with JPA and data classes](https://blog.codecentric.de/en/2017/06/kotlin-spring-working-jpa-data-classes/)
* [Auditing with JPA, Hibernate, and Spring Data JPA](https://www.baeldung.com/database-auditing-jpa)
* [Spring Boot and H2 in memory database - Why, What and How?](http://www.springboottutorial.com/spring-boot-and-h2-in-memory-database)

## Clean Architecture

* [A Guided Tour inside a clean architecture code base.](https://proandroiddev.com/a-guided-tour-inside-a-clean-architecture-code-base-48bb5cc9fc97)

## Domain-Driven Design

* [Entities, Value Objects, Aggregates and Roots](https://lostechies.com/jimmybogard/2008/05/21/entities-value-objects-aggregates-and-roots/)
* [Domain-Driven Design and Spring](http://static.olivergierke.de/lectures/ddd-and-spring/)

## GitLab

* [GitLab Runner Commands](https://docs.gitlab.com/runner/commands/README.html)
* [GitLab CI/CD Variables](https://docs.gitlab.com/ee/ci/variables/)
* [Continuous delivery of a Spring Boot application with GitLab CI and Kubernetes](https://about.gitlab.com/2016/12/14/continuous-delivery-of-a-spring-boot-application-with-gitlab-ci-and-kubernetes/)
* [Configuration of your jobs with .gitlab-ci.yml: cache-policy](https://docs.gitlab.com/ee/ci/yaml/#cache-policy)

## Docker

* [Dockerize](https://github.com/jwilder/dockerize)


## Docker Compose based Integration Testing

* [INTEGRATION TESTING WITH DOCKER-COMPOSE, GRADLE AND SPRING BOOT](https://info.michael-simons.eu/2017/11/20/integration-testing-with-docker-compose-gradle-and-spring-boot/)
* [A JUnit(4) rule to manage docker containers using docker-compose](https://github.com/palantir/docker-compose-rule)
* [gradle-docker-compose-plugin](https://github.com/avast/gradle-docker-compose-plugin)

# Unrelated Opinion Pieces

* [What I’ve learned so far about software development](https://medium.com/@landongn/12-years-later-what-i-ve-learned-about-being-a-software-engineer-d6e334d6e8a3)
