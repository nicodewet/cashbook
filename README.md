# cashbook

[![Sonarcloud Status](https://sonarcloud.io/api/project_badges/measure?project=cashbook&metric=alert_status)](https://sonarcloud.io/dashboard?id=cashbook)

An application (API) that helps you categorise your transactions and monitor GST.

This project has been inspired by the [New Zealand Inland Revenue Department (IRD) cashbook template](https://www.ird.govt.nz/tool-for-business/downloads/downloads-index.html) and
enhances the template in a number of ways:

* Transactions can be linked into source-target relationships
* Future IRD mandated (compulsory) transactions can be recorded along with the source-target relationship

Recording future compulsory transactions is a form of simple workflow, it facilitates not having to remember
or calculate payments due to the IRD when it comes to particular types of transactions (in particular GST
payments).

A key goal is that the application should always provide a report of payments due (debts) and
when they are due so you don't need to think or worry about this as a business owner.

## examples

### basic example - add invoice payment

```shell
$ curl -d '{"type":"INVOICE_PAYMENT","targetTransactionUUID":null,"scheduledDate":null,"completedDate":"2018-06-20","amountInCents":23000,"gstInCents":0,"evidenceLink":null}' -H "Content-Type: application/json" -X POST http://localhost:8080/api/business/transactions
$ curl -sS 'http://localhost:8080/api/business/transactions?period=2018-06'
$ curl -sS 'http://localhost:8080/api/business/transactions/93e6f203-d516-4e45-ac6c-2777d3a66d56'
```

### linked transaction

### option A - target added first

Here we add a scheduled GST payment (not yet complete i.e. payment has not yet been made) and associated invoice 
payment transactions.

```shell
$ curl -d '{"type":"IRD_GST_PAYMENT","targetTransactionUUID":null,"scheduledDate":"2018-10-29","completedDate":null,"amountInCents":23000,"gstInCents":0,"evidenceLink":null}' -H "Content-Type: application/json" -X POST http://localhost:8080/api/business/transactions | jsonpp
{
  "uuid": "9175cd3a-6951-4e14-9be5-23d2e1fa297c",
  "validationErrorMessage": null
}
$ curl -d '{"type":"INVOICE_PAYMENT","targetTransactionUUID":"9175cd3a-6951-4e14-9be5-23d2e1fa297c","scheduledDate":null,"completedDate":"2018-06-20","amountInCents":23000,"gstInCents":0,"evidenceLink":null}' -H "Content-Type: application/json" -X POST http://localhost:8080/api/business/transactions
{
  "uuid": "ef6ab688-38e4-4263-8a2b-a9e36bedbfbb",
  "validationErrorMessage": null
}
$ curl -sS 'http://localhost:8080/api/business/transactions/9175cd3a-6951-4e14-9be5-23d2e1fa297c' | jsonpp
{
  "uuid": "9175cd3a-6951-4e14-9be5-23d2e1fa297c",
  "createdTimestamp": "2018-10-26T08:09:14.064",
  "lastUpdateTimestamp": "2018-10-26T08:09:14.149",
  "scheduledDate": "2018-10-29",
  "type": "IRD_GST_PAYMENT",
  "amountInCents": 23000,
  "gstInCents": 0,
  "sourceTransactions": [
    {
      "uuid": "ef6ab688-38e4-4263-8a2b-a9e36bedbfbb",
      "createdTimestamp": "2018-10-26T08:12:10.143",
      "lastUpdateTimestamp": "2018-10-26T08:12:10.149",
      "completedDate": "2018-06-20",
      "type": "INVOICE_PAYMENT",
      "amountInCents": 23000,
      "gstInCents": 0,
      "businessTransactionIssue": "INVOICE_PAYMENT_WITH_INCORRECT_GST",
      "businessTransactionIssueDetail": "GST of 3450 expected"
    }
  ],
  "businessTransactionIssue": "NONE"
}
```

### option B - target added after source

```shell
$ curl -d '{"type":"INVOICE_PAYMENT","completedDate":"2018-06-20","amountInCents":23000}' -H "Content-Type: application/json" -X POST http://localhost:8080/api/business/transactions
{
    "uuid": "3264fe89-3ebd-4b2f-aedd-d8adbc50b257",
    "validationErrorMessage": null
}
$ curl -d '{"type":"IRD_GST_PAYMENT","scheduledDate":"2018-10-31"}' -H "Content-Type: application/json" -X POST http://localhost:8080/api/business/transactions
{
    "uuid": "4669e6eb-0a84-427b-b5fb-9e9f0ade0f0a",
    "validationErrorMessage": null
}
$ curl -d '{"sourceTransactionUuid":"3264fe89-3ebd-4b2f-aedd-d8adbc50b257","targetTransactionUuid":"4669e6eb-0a84-427b-b5fb-9e9f0ade0f0a"}' -H "Content-Type: application/json" -X POST http://localhost:8080/api/business/transactions/link | jsonpp
{
  "uuid": "4669e6eb-0a84-427b-b5fb-9e9f0ade0f0a",
  "createdTimestamp": "2018-10-29T16:40:32.83",
  "lastUpdateTimestamp": "2018-10-29T16:40:32.835",
  "scheduledDate": "2018-10-31",
  "type": "IRD_GST_PAYMENT",
  "amountInCents": 0,
  "gstInCents": 0,
  "sourceTransactions": [
    {
      "uuid": "3264fe89-3ebd-4b2f-aedd-d8adbc50b257",
      "createdTimestamp": "2018-10-29T16:38:36.566",
      "lastUpdateTimestamp": "2018-10-29T16:50:34.415",
      "completedDate": "2018-06-20",
      "type": "INVOICE_PAYMENT",
      "amountInCents": 23000,
      "gstInCents": 0,
      "businessTransactionIssue": "INVOICE_PAYMENT_WITH_INCORRECT_GST",
      "businessTransactionIssueDetail": "GST of 3450 expected"
    }
  ],
  "businessTransactionIssue": "NONE"
}
```

## build

Ultimately the sure-fire build process is dictated by our *One step build with a CI image* build steps which you can 
reliably run on your local machine. 

If your only interest is in a reliable build and shipping process then skip ahead.

On your local machine, you would generally do the following from the command line or from your IDE:

```shell
$ ./gradlew build
```

### active spring profiles and changing configuration depending on the environment

[Spring Profiles](https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-profiles.html) are used
to change configuration depending on the environment. The cited Spring Profiles is used in combination with Spring's 
ability to use 
[YAML for external properties](https://docs.spring.io/spring-boot/docs/current/reference/html/howto-properties-and-configuration.html).

Please see src/main/application.yml

In test cases that leverage testcontainers we use the
[ActiveProfiles](https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/test/context/ActiveProfiles.html) 
annotation to choose the appropriate profile. So we're selecting configuration from the src/main/application.yml file. 

#### testcontainers profile

This profile is used to make sure integration test cases are configured appropriately and in addition we want to isolate
this phase of testing in our pipeline (see .gitlab-ci.yml for pipeline configuration).

```shell
$ ./gradlew test -Dspring.profiles.active=testcontainers
```

Forcing a re-run:

```shell
$ ./gradlew cleanTest test -Dspring.profiles.active=testcontainers
```

## code quality

This project uses [SonarQube](https://sonarqube.org/) which 
[introduced support for Kotlin in version 7.3](https://www.sonarqube.org/sonarqube-7-3/) 
onwards.

If you want to experiment on your local here is an example of the command you'll need to run (change the login token):

```shell
 ./gradlew sonarqube -Dsonar.host.url=http://localhost:9000 -Dsonar.login=4182970e4cc7160a223196b1b56939029cca4816
```

## testcontainers

This project uses [Testcontainers](https://www.testcontainers.org/) along with it's 
[JUnit 5 support](https://www.testcontainers.org/test_framework_integration/junit_5/). Due to Kotlin 
support issues these test cases are written in Java and so can be found in the src/test/java directory. 

## shipping

### localhost

Shipping means wrapping our spring boot application with docker engine imaging and using the gradle parts of
the [Spring Boot with Docker guide](https://spring.io/guides/gs/spring-boot-docker/) (the overarching philosophy 
being to always follow the spring guides as a starting point).

Notice that the [palantir gradle-docker plugin](https://github.com/palantir/gradle-docker) is used and that to build 
the *latest* image on your local you would execute the *docker* task from the command line or from your IDE:

```shell
$ ./gradlew docker
```

If you had pruned all your local images ($ docker system prune -a) prior to executing the docker build task your local 
images should look something like what is shown below. 

```shell
$ docker images
REPOSITORY             TAG                 IMAGE ID            CREATED             SIZE
com.thorgil/cashbook   latest              35b4a1d29319        47 seconds ago      123MB
openjdk                8-jdk-alpine        5801f7d008e5        6 weeks ago         103MB

```

### CI server

#### Two step build

We have a two step process and have deliberately chosen not to use a multistage build process. We are also accepting 
that the first version of the process will not be perfect (*we'll get there in time*). 

The first step is to build the spring boot jar using an [official openjdk build image](https://hub.docker.com/_/openjdk/).
This will be significantly slower than your local build process because the build dependencies will not be cached on the 
build image (in time this can and should be attended to).

Note what we have gained here is that the build command is our authoritative reference in the sense that as a developer
it tells you that if you are building on your local make sure you are using JDK 8.

Note it is important that we use the gradle wrapper (./gradlew) below otherwise there is no point in having the gradle
directory in version control.

```shell
$ docker run --rm -v "$PWD":/home/gradle/project -w /home/gradle/project openjdk:8-jdk-alpine ./gradlew build
$ docker build -t com.thorgil/cashbook:latest --build-arg JAR_FILE=build/libs/cashbook-0.0.1-SNAPSHOT.jar .
```

Apart from being slow, the above build process is suboptimal because we are violating the Don't-Repeat-Yourself (DRY)
principle and also the process is not as robust as it could be. The explanations for each is provided below.

* We are violating the DRY principle because the docker build step and associated configuration has already been done in
our build.gradle file.
* The process not as robust as it could be because the JAR_FILE reference is subject to change when we do a 
release.

In short, our build server process should be exactly the same as our local process ($ ./gradlew docker) - that is the 
gist of the problem we'll solve next by creating our own CI image and using it to reduce the docker image build to 
one step.

#### One step build with a CI image

Our goal with a one step CI image is to execute the exact same command, namely *./gradlew docker* that we execute on
our local machines as developers.

##### build our CI image

```shell
docker build -f Builder.Dockerfile . -t com.thorgil/cashbook-builder:latest
```

##### expose the Docker socket to our CI container

Here we expose the Docker socket to our CI container, by bind-mounting it with the -v flag, as 
[has been advised](http://jpetazzo.github.io/2015/09/03/do-not-use-docker-in-docker-for-ci/).

```shell
docker run -v /var/run/docker.sock:/var/run/docker.sock -ti com.thorgil/cashbook-builder:latest
```

Execute say #docker ps or #docker images to satisfy yourself that you are talking to Docker Engine on the host (i.e. 
this is *not* Docker in Docker).

##### build the image on the CI server in one step

```shell
docker run --rm -v "$PWD":/home/gradle/project -v /var/run/docker.sock:/var/run/docker.sock -w /home/gradle/project com.thorgil/cashbook-builder:latest ./gradlew docker
```

Once that process is done the image will reside on the host machine and can be pushed to a registry from there or as
a part of the previous step.