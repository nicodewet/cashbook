package com.thorgil.cashbook.dataproviders;

import com.thorgil.cashbook.dataproviders.entity.BusinessTransactionEntity;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * These tests have been written in Java and not Kotlin due to the following issue:
 * https://github.com/testcontainers/testcontainers-java/issues/238
 */
@ExtendWith(SpringExtension.class)
@Testcontainers
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("testcontainers")
class TransactionsWithPostgresContainerTest {

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(TransactionsWithPostgresContainerTest.class);

    @Autowired
    BusinessTransactionEntityRepository repo;

    @Autowired
    JdbcTemplate jdbcTemplate;

    // will be started before and stopped after each makeAbsolutelySureWeAreConnectedToPostgreSQL method
    @Container
    private static final PostgreSQLContainer postgresqlContainer = new PostgreSQLContainer()
            .withDatabaseName("cashbook")
            .withUsername("postgres")
            .withPassword("postgres");
    @Test
    void makeAbsolutelySureWeAreConnectedToPostgreSQL() {
        assertTrue(postgresqlContainer.isRunning());

        Optional<BusinessTransactionEntity> optTrans = repo.findById("foo");
        assertThat(optTrans.isPresent()).isFalse();

        Integer result = jdbcTemplate.queryForObject("SELECT COUNT(*) from pg_database", Integer.class);
        assertThat(result).isGreaterThan(0);

        List<String> data = jdbcTemplate.query("SELECT * from pg_database", new RowMapper<String>(){
            public String mapRow(ResultSet rs, int rowNum)
                    throws SQLException {
                return rs.getString(1);
            }
        });
        data.forEach(val -> LOGGER.info("pg_database: " + val));
        assertThat(data.size()).isGreaterThan(0);
    }

}
