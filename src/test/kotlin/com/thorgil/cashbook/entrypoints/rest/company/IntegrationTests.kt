package com.thorgil.cashbook.entrypoints.rest.company

import com.thorgil.cashbook.core.entity.BusinessTransactionType
import com.thorgil.cashbook.entrypoints.rest.business.transaction.*
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.test.web.client.postForEntity
import org.springframework.boot.test.web.client.postForObject
import org.springframework.core.ParameterizedTypeReference
import org.springframework.test.context.junit.jupiter.SpringExtension
import java.time.LocalDate
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.http.*

@ExtendWith(SpringExtension::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class IntegrationTests(@Autowired val restTemplate: TestRestTemplate) {

    @BeforeAll
    fun setup() {
        println(">> Setup")
    }

    @LocalServerPort
    private val port: Int = 0

    @Test
    fun `Assert can post BusinessTransaction and then fetch it by uuid`() {

        val addBusinessTransactionPostBody = AddBusinessTransactionPostBody(type = BusinessTransactionType.INVOICE_PAYMENT,
                completedDate = LocalDate.parse("2018-07-31"), amountInCents = 23000)

        val request = HttpEntity(addBusinessTransactionPostBody)

        val response = restTemplate.postForObject(BusinessTransactionApiEndpoint.BUSINESS_TRANSACTION_END_POINT_URL, request, AddBusinessTransactionResponse::class.java)

        assertThat(response).isNotNull
        assertThat(response.uuid).isNotNull()
        assertThat(response.validationErrorMessage).isNull()

        val getTransResponse = restTemplate.getForEntity(BusinessTransactionApiEndpoint.BUSINESS_TRANSACTION_END_POINT_URL + "/"
                + response.uuid,
                BusinessTransactionDTO::class.java)

        assertThat(getTransResponse.statusCode).isEqualTo(HttpStatus.OK)

    }

    @Test
    fun `Assert can post BusinessTransactions in particular month and then fetch them`() {

        // Arrange 1 - transaction with completedDate in e.g. July '18

        val completedDate = LocalDate.now()
        val addBusinessTransactionPostBody1 = AddBusinessTransactionPostBody(type = BusinessTransactionType.INVOICE_PAYMENT,
                completedDate = completedDate, amountInCents = 23000)

        val request1 = HttpEntity(addBusinessTransactionPostBody1)

        val response1 = restTemplate.postForObject(BusinessTransactionApiEndpoint.BUSINESS_TRANSACTION_END_POINT_URL, request1, AddBusinessTransactionResponse::class.java)

        assertThat(response1).isNotNull
        assertThat(response1.uuid).isNotNull()
        assertThat(response1.validationErrorMessage).isNull()

        // Arrange 2 - transaction with scheduledDate in e.g. July '18

        val addBusinessTransactionPostBody2 = AddBusinessTransactionPostBody(type = BusinessTransactionType.INVOICE_PAYMENT,
                scheduledDate = completedDate, amountInCents = 23000)

        val request2 = HttpEntity(addBusinessTransactionPostBody2)

        val response2 = restTemplate.postForObject(BusinessTransactionApiEndpoint.BUSINESS_TRANSACTION_END_POINT_URL, request2, AddBusinessTransactionResponse::class.java)

        assertThat(response2).isNotNull
        assertThat(response2.uuid).isNotNull()
        assertThat(response2.validationErrorMessage).isNull()

        val respType = object: ParameterizedTypeReference<BusinessTransactionResponseDTO>(){}
        val headers = HttpHeaders()
        val entity = HttpEntity<String>(null, headers)

        // Act
        var month = completedDate.month.value.toString()
        if (month.length == 1) {
            month = "0" + month
        }
        val getResponse = restTemplate.exchange(
                createURLWithPort(BusinessTransactionApiEndpoint.BUSINESS_TRANSACTION_END_POINT_URL + "?period=" + completedDate.year + "-" + month),
                HttpMethod.GET, entity, respType)

        // Assert

        assertThat(getResponse.body).isNotNull

        // the uuid of each expected trans appeared there may in fact end up being more than 2 because the DB is shared
        // between tests (suboptimal, to be fixed)
        assertThat(getResponse.body?.transactions?.size).isGreaterThanOrEqualTo(2)

        var firstResponseFound = false
        var secondResponseFound = false

        val trans = getResponse.body?.transactions
        if (trans != null) {
            for (foundTransaction in trans) {
                if (foundTransaction.uuid == response1.uuid) {
                    firstResponseFound = true
                } else if (foundTransaction.uuid == response2.uuid) {
                    secondResponseFound = true
                }
            }
        } else {
            assert(false)
        }
        assertThat(firstResponseFound).isTrue()
        assertThat(secondResponseFound).isTrue()
    }

    @Test
    fun `Assert can post target and source BusinessTransactions and link shows up when target fetched by uuid`() {

        // [TARGET] Arrange 1 - Expense - Add scheduled IRD GST payment e.g. entity is GST registered and so we know
        // when these will occur based on IRD rules.
        val scheduledGstPaymentTransaction = AddBusinessTransactionPostBody(type = BusinessTransactionType.IRD_GST_PAYMENT,
                scheduledDate = LocalDate.now().plusMonths(6), amountInCents = 0, gstInCents = 0)

        val scheduledGstPaymentTransactionHttpReq = HttpEntity(scheduledGstPaymentTransaction)

        val postSchedPaymentResponse = restTemplate.postForObject(BusinessTransactionApiEndpoint.BUSINESS_TRANSACTION_END_POINT_URL,
                                    scheduledGstPaymentTransactionHttpReq,
                                    AddBusinessTransactionResponse::class.java)

        assertThat(postSchedPaymentResponse).isNotNull
        assertThat(postSchedPaymentResponse.uuid).isNotNull()
        assertThat(postSchedPaymentResponse.validationErrorMessage).isNull()

        // [SOURCE] Arrange 2 - Income - Invoice payment.
        val invoicePaymentTransaction = AddBusinessTransactionPostBody(type = BusinessTransactionType.INVOICE_PAYMENT,
                completedDate = LocalDate.now(), amountInCents = 23000, gstInCents = 3450,
                targetTransactionUUID = postSchedPaymentResponse.uuid)

        val invoicePaymentTransHttpReq = HttpEntity(invoicePaymentTransaction)
        val invoicePaymentResponse = restTemplate.postForObject(BusinessTransactionApiEndpoint.BUSINESS_TRANSACTION_END_POINT_URL,
                invoicePaymentTransHttpReq,
                AddBusinessTransactionResponse::class.java)

        assertThat(invoicePaymentResponse).isNotNull
        assertThat(invoicePaymentResponse.uuid).isNotNull()
        assertThat(invoicePaymentResponse.validationErrorMessage).isNull()

        // Act

        val getTargetResponse = restTemplate.getForEntity(BusinessTransactionApiEndpoint.BUSINESS_TRANSACTION_END_POINT_URL + "/"
                                                                                                + postSchedPaymentResponse.uuid,
                                                                    BusinessTransactionDTO::class.java)

        // Assert
        assertThat(getTargetResponse.statusCode).isEqualTo(HttpStatus.OK)
        assertThat(getTargetResponse.body).isNotNull
        assertThat(getTargetResponse.body?.sourceTransactions).isNotNull
        assertThat(getTargetResponse.body?.sourceTransactions?.size).isEqualTo(1)
        if (getTargetResponse.body != null) {
           val dtoList: List<BusinessTransactionDTO>? = getTargetResponse.body?.sourceTransactions
            if (dtoList != null) {
                val source: BusinessTransactionDTO = dtoList[0]
                assertThat(source.uuid).isEqualTo(invoicePaymentResponse.uuid)
            }
        }

    }

    @Test
    fun `Assert can link previously created source and target BusinessTransactions`() {

        // [TARGET] Arrange 1 - Expense - Add scheduled IRD GST payment e.g. entity is GST registered and so we know
        // when these will occur based on IRD rules.
        val scheduledGstPaymentTransaction = AddBusinessTransactionPostBody(type = BusinessTransactionType.IRD_GST_PAYMENT,
                scheduledDate = LocalDate.now().plusMonths(6), amountInCents = 0, gstInCents = 0)

        val scheduledGstPaymentTransactionHttpReq = HttpEntity(scheduledGstPaymentTransaction)

        val postSchedPaymentResponse = restTemplate.postForObject(BusinessTransactionApiEndpoint.BUSINESS_TRANSACTION_END_POINT_URL,
                scheduledGstPaymentTransactionHttpReq,
                AddBusinessTransactionResponse::class.java)

        assertThat(postSchedPaymentResponse).isNotNull
        assertThat(postSchedPaymentResponse.uuid).isNotNull()
        assertThat(postSchedPaymentResponse.validationErrorMessage).isNull()

        // [SOURCE] Arrange 2 - Income - Invoice payment.
        val invoicePaymentTransaction = AddBusinessTransactionPostBody(type = BusinessTransactionType.INVOICE_PAYMENT,
                completedDate = LocalDate.now(), amountInCents = 23000, gstInCents = 3450)

        val invoicePaymentTransHttpReq = HttpEntity(invoicePaymentTransaction)
        val invoicePaymentResponse = restTemplate.postForObject(BusinessTransactionApiEndpoint.BUSINESS_TRANSACTION_END_POINT_URL,
                invoicePaymentTransHttpReq,
                AddBusinessTransactionResponse::class.java)

        assertThat(invoicePaymentResponse).isNotNull
        assertThat(invoicePaymentResponse.uuid).isNotNull()
        assertThat(invoicePaymentResponse.validationErrorMessage).isNull()

        // Act
        val linkBody: LinkBusinessTransactionPostBody = LinkBusinessTransactionPostBody(invoicePaymentResponse.uuid, postSchedPaymentResponse.uuid)
        val linkPostBody = HttpEntity(linkBody)
        val url = BusinessTransactionApiEndpoint.BUSINESS_TRANSACTION_END_POINT_URL + "/" + BusinessTransactionApiEndpoint.LINK_TRANSACTIONS_REQUEST_RESOURCE
        val response: ResponseEntity<BusinessTransactionDTO> = restTemplate.postForEntity(url, linkPostBody, BusinessTransactionDTO::class.java)
        assertThat(response).isNotNull
        assertThat(response.statusCode).isEqualTo(HttpStatus.OK)
        assertThat(response.body?.sourceTransactions).isNotNull
        assertThat(response.body?.sourceTransactions?.size).isEqualTo(1)
    }

    private fun createURLWithPort(uri: String): String {
        return "http://localhost:$port$uri"
    }


    @AfterAll
    fun teardown() {
        println(">> Tear down")
    }
}