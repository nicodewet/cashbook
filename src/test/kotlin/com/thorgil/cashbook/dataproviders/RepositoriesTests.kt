package com.thorgil.cashbook.dataproviders

import com.thorgil.cashbook.dataproviders.entity.BusinessTransactionEntity
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.assertNotNull
import java.time.*
import java.time.format.DateTimeFormatter
import java.util.*

@ExtendWith(SpringExtension::class)
@DataJpaTest
class RepositoriesTests(@Autowired val entityManager: TestEntityManager,
                        @Autowired val businessTransactionRepo: BusinessTransactionEntityRepository) {

    companion object {
        val TYPE = "INVOICE"
    }

    @Test
    fun `When findById then return BusinessTransactionEntity`() {

        val trans = BusinessTransactionEntity(uuid = UUID.randomUUID().toString(),
                                            type = TYPE,
                                            gstInCents = 0,
                                            amountInCents = 100)
        entityManager.persist(trans)
        entityManager.flush()

        val found = businessTransactionRepo.findById(trans.uuid)
        val foundTrans = found.get()

        assertNotNull(foundTrans)

        assertThat(foundTrans.uuid).isEqualTo(trans.uuid)
        assertThat((foundTrans.createdAt)).isNotNull()
        assertThat(foundTrans.updatedAt).isNotNull()
        assertThat(foundTrans.type).isEqualTo(TYPE)
        assertThat(foundTrans.gstInCents).isEqualTo(trans.gstInCents)
        assertThat(foundTrans.amountInCents).isEqualTo(trans.amountInCents)
        assertThat(foundTrans.evidenceLink).isNull()
        assertThat(foundTrans.target).isNull()
        assertThat(foundTrans.sources).isNotNull
    }

    @Test
    fun `When target specified association is persisted when fetching target and source`() {

        val target = BusinessTransactionEntity(uuid = UUID.randomUUID().toString(),
                                                type = TYPE,
                                                gstInCents = 0,
                                                amountInCents = 100)

        val source = BusinessTransactionEntity(uuid = UUID.randomUUID().toString(),
                                                 type = TYPE,
                                                 gstInCents = 0,
                                                 amountInCents = 100,
                                                 target = target)
        target.sources.add(target)

        val persistedParent = entityManager.persistAndFlush(target)
        val persistedChild = entityManager.persistAndFlush(source)

        assertThat(persistedParent.sources.size).isEqualTo(1)
        assertThat(persistedChild.target?.uuid).isEqualTo(target.uuid)
    }

    @Test
    fun `When BusinessTransactionEntity with completedDate exists we are able to find it`() {

        // Arrange
        val formatter: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy MM dd")
        val completedDate: LocalDate = LocalDate.parse("2018 09 21", formatter)

        val trans = BusinessTransactionEntity(uuid = UUID.randomUUID().toString(),
                                              type = TYPE,
                                              gstInCents = 0,
                                              amountInCents = 100,
                                              completedDate = completedDate)
        entityManager.persistAndFlush(trans)

        // Act
        val found = businessTransactionRepo.findByCompletedDateOrScheduledDate(completedDate, null)

        // Assert
        assertNotNull(found)
        assertThat(found?.size).isEqualTo(1)

    }

    @Test
    fun `When BusinessTransactionEntity with completedDate or scheduledDate exists we are able to find it`() {

        // Arrange
        val formatter: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy MM dd")
        val completedDate: LocalDate = LocalDate.parse("2018 09 21", formatter)

        val completedTrans = BusinessTransactionEntity(uuid = UUID.randomUUID().toString(),
                type = TYPE,
                gstInCents = 0,
                amountInCents = 100,
                completedDate = completedDate)

        entityManager.persistAndFlush(completedTrans)

        val scheduledDate: LocalDate = LocalDate.parse("2018 09 21", formatter)

        val scheduledTrans = BusinessTransactionEntity(uuid = UUID.randomUUID().toString(),
                type = TYPE,
                gstInCents = 0,
                amountInCents = 100,
                scheduledDate = scheduledDate)

        entityManager.persistAndFlush(scheduledTrans)

        // Act
        val found = businessTransactionRepo.findByCompletedDateOrScheduledDate(completedDate, scheduledDate)

        // Assert
        assertNotNull(found)
        assertThat(found?.size).isEqualTo(2)

    }

    @Test
    fun `Given a particular YearMonth, able to find all transactions by createDate that falls into that YearMonth`() {

        // Arrange
        val trans = BusinessTransactionEntity(uuid = UUID.randomUUID().toString(),
                                                type = TYPE, gstInCents = 0, amountInCents = 100)
        val now: LocalDateTime = LocalDateTime.now()
        trans.createdAt = LocalDateTime.now()
        entityManager.persistAndFlush(trans)

        // Act
        val initialYearMonth: YearMonth = YearMonth.now()
        val startOfMonth: LocalDateTime = DateUtil.getStartTimeStamp(initialYearMonth)
        val endOfMonth: LocalDateTime = DateUtil.getEndTimeStamp(initialYearMonth)
        val found = businessTransactionRepo.findByCreatedAtBetween(startOfMonth, endOfMonth)

        // Assert
        assertNotNull(found)
        assertThat(found?.size).isEqualTo(1)
    }

    @Test
    fun `Given a particular YearMonth, able to find all transactions by completedDate that falls into that YearMonth`() {

        // Arrange
        val trans = BusinessTransactionEntity(uuid = UUID.randomUUID().toString(),
                type = TYPE, gstInCents = 0, amountInCents = 100, completedDate = LocalDate.now())

        entityManager.persistAndFlush(trans)

        // Act
        val initialYearMonth: YearMonth = YearMonth.now()
        val startOfMonth: LocalDateTime = DateUtil.getStartTimeStamp(initialYearMonth)
        val endOfMonth: LocalDateTime = DateUtil.getEndTimeStamp(initialYearMonth)
        val found = businessTransactionRepo.findByCompletedDateBetween(startOfMonth.toLocalDate(), endOfMonth.toLocalDate())

        // Assert
        assertNotNull(found)
        assertThat(found?.size).isEqualTo(1)
    }

    @Test
    fun `Given a particular YearMonth, able to find all transactions by scheduledDate that falls into that YearMonth`() {

        // Arrange
        val trans = BusinessTransactionEntity(uuid = UUID.randomUUID().toString(),
                type = TYPE, gstInCents = 0, amountInCents = 100, scheduledDate = LocalDate.now())

        entityManager.persistAndFlush(trans)

        // Act
        val initialYearMonth: YearMonth = YearMonth.now()
        val startOfMonth: LocalDateTime = DateUtil.getStartTimeStamp(initialYearMonth)
        val endOfMonth: LocalDateTime = DateUtil.getEndTimeStamp(initialYearMonth)
        val found = businessTransactionRepo.findByScheduledDateBetween(startOfMonth.toLocalDate(), endOfMonth.toLocalDate())

        // Assert
        assertNotNull(found)
        assertThat(found?.size).isEqualTo(1)
    }

    @Test
    fun `Can associate target and source business transactions after they had already been added`() {

        // Arrange

        val source = BusinessTransactionEntity(uuid = UUID.randomUUID().toString(),
                type = TYPE, gstInCents = 0, amountInCents = 100, scheduledDate = LocalDate.now())

        val target = BusinessTransactionEntity(uuid = UUID.randomUUID().toString(),
                type = TYPE, gstInCents = 0, amountInCents = 100, scheduledDate = LocalDate.now())

        val persistedSource = entityManager.persistAndFlush(source)
        assertThat(persistedSource).isNotNull
        val persistedTarget = entityManager.persistAndFlush(target)
        assertThat(persistedTarget).isNotNull

        //  Act

        persistedSource.addTarget(persistedTarget)
        val updatedSource = entityManager.persistAndFlush(persistedSource)

        val updatedTarget = businessTransactionRepo.findById(target.uuid)

        // Assert

        assertThat(updatedSource).isNotNull
        assertThat(updatedTarget.isPresent).isTrue()
        assertThat(updatedSource.target?.uuid).isEqualTo(updatedTarget.get().uuid)
        assertThat(updatedTarget.get().sources).isNotNull
        assertThat(updatedTarget.get().sources.size).isEqualTo(1)
    }
}