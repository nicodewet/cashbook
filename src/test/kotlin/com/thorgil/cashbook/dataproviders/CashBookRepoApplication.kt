package com.thorgil.cashbook.dataproviders

import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class CashBookRepoApplication {
}