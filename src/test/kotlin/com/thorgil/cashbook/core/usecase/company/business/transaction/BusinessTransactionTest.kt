package com.thorgil.cashbook.core.usecase.company.business.transaction

import com.thorgil.cashbook.core.entity.*
import com.thorgil.cashbook.core.usecase.business.transaction.invariant.BusinessTransactionErrorCode
import com.thorgil.cashbook.core.usecase.business.transaction.invariant.BusinessTransactionException
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import java.time.LocalDate
import java.time.Month

class BusinessTransactionTest {

    companion object {
        val companyConfiguration = CompanyConfiguration("Thorgil Limited",
                "Thorgil Software",
                "6833053",
                "9429046765208",
                LocalDate.now(),
                Month.JUNE,
                GstStatus.REGISTERED,
                LocalDate.now(),
                "125-814-972")
    }

    @Test
    fun `when a source transaction has a completed date AFTER the target transaction an exception is thrown`() {

        // Arrange

        val sourceIncomeTransaction = BusinessTransaction(type = BusinessTransactionType.INVOICE_PAYMENT,
                amountInCents = 0,
                scheduledDate = null,
                completedDate = LocalDate.now().minusMonths(1),
                gstInCents = 0,
                sourceTransactions = hashSetOf(),
                companyConfig = companyConfiguration)

        // Act

        val exception = Assertions.assertThrows(BusinessTransactionException::class.java) {
            val targetGstPaymentTransaction = BusinessTransaction(type = BusinessTransactionType.INVOICE_PAYMENT,
                    amountInCents = 0,
                    scheduledDate = null,
                    completedDate = LocalDate.now().minusMonths(2),
                    gstInCents = 0,
                    sourceTransactions = hashSetOf(sourceIncomeTransaction),
                    companyConfig = companyConfiguration)
        }

        // Assert

        assertThat(exception.errorCode).isEqualTo(BusinessTransactionErrorCode.SOURCE_COMPLETED_DATE_AFTER_TARGET_COMPLETED_DATE)
    }

    @Test
    fun `when the source and target transaction are the same entity because a matching UUID an exception is thrown`() {

        // Arrange

        val sourceIncomeTransaction = BusinessTransaction(type = BusinessTransactionType.INVOICE_PAYMENT,
                amountInCents = 0,
                scheduledDate = null,
                completedDate = LocalDate.now().minusMonths(2),
                gstInCents = 0,
                sourceTransactions = hashSetOf(),
                companyConfig = companyConfiguration)

        // Act

        val exception = Assertions.assertThrows(BusinessTransactionException::class.java) {
            val targetGstPaymentTransaction = BusinessTransaction(uuid = sourceIncomeTransaction.uuid,
                    type = BusinessTransactionType.INVOICE_PAYMENT,
                    amountInCents = 0,
                    scheduledDate = null,
                    completedDate = LocalDate.now().minusMonths(1),
                    gstInCents = 0,
                    sourceTransactions = hashSetOf(sourceIncomeTransaction),
                    companyConfig = companyConfiguration)
        }

        // Assert

        assertThat(exception.errorCode).isEqualTo(BusinessTransactionErrorCode.CHILD_SAME_AS_THIS_ENTITY_LOGICAL_ERROR)
    }

    @Test
    fun `transaction scheduled date cannot be for a past date but this is a mere issue, not an exception to allow for reconstitution from storage`() {

        // Arrange & Act

        val sourceIncomeTransaction = BusinessTransaction(type = BusinessTransactionType.INVOICE_PAYMENT,
                amountInCents = 0,
                scheduledDate = LocalDate.now().minusMonths(2),
                completedDate = null,
                gstInCents = 0,
                sourceTransactions = hashSetOf(),
                companyConfig = companyConfiguration)

        // Assert

        assertThat(sourceIncomeTransaction.businessTransactionIssue).isEqualTo(BusinessTransactionIssue.SCHEDULED_DATE_MUST_BE_FOR_A_FUTURE_DATE)

    }
}