package com.thorgil.cashbook.core.usecase.company.business.transaction

import com.thorgil.cashbook.core.entity.BusinessTransaction
import com.thorgil.cashbook.core.entity.BusinessTransactionType
import com.thorgil.cashbook.core.entity.CompanyConfiguration
import com.thorgil.cashbook.core.entity.GstStatus
import com.thorgil.cashbook.core.usecase.business.transaction.*
import com.thorgil.cashbook.core.usecase.business.transaction.invariant.BusinessTransactionErrorCode
import com.thorgil.cashbook.core.usecase.business.transaction.invariant.BusinessTransactionException
import com.thorgil.cashbook.core.usecase.company.GetCompany
import com.thorgil.cashbook.dataproviders.BusinessTransactionDataProvider
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.slf4j.LoggerFactory
import java.time.LocalDate
import java.time.Month
import java.time.YearMonth
import java.util.*

class AddLinkedBusinessTransactionUseCaseTest {

    companion object {

        private val LOGGER = LoggerFactory.getLogger(AddStandAloneBusinessTransactionUseCaseTest::class.java)

        private  val companyProvider: GetCompany = object: GetCompany {
            override fun getCompany(): CompanyConfiguration {
                return CompanyConfiguration(entityName = "ABC Limited",
                        companyNumber = "123",
                        NZBN = "123",
                        incorporationDate = LocalDate.now(),
                        annualReturnFilingMonth = Month.JUNE,
                        gstStatus = GstStatus.REGISTERED)
            }
        }

        val transactions: HashMap<String, BusinessTransaction> = HashMap()

        private val addBusinessTransactionInRepo: AddBusinessTransactionInRepository = object: AddBusinessTransactionInRepository {
            override fun addBusinessTransactionInRepository(businessTransaction: BusinessTransaction, targetTransactionUuid: String?) {
                if (targetTransactionUuid != null) {
                    val target = transactions[targetTransactionUuid]
                    if (target != null) {
                        transactions[target.uuid] = target.createCopyWithAddedSourceTransaction(businessTransaction)
                    } else {
                        throw BusinessTransactionException(BusinessTransactionErrorCode.TARGET_DOES_NOT_EXIST,
                                BusinessTransactionDataProvider.TARGET_OBJECT_DOES_NOT_EXIST_MESSAGE)
                    }
                }
                transactions[businessTransaction.uuid] = businessTransaction
            }

        }

        private val fetchBusinessTransactionsFromRepo: FetchBusinessTransactionsFromRepository = object: FetchBusinessTransactionsFromRepository {
            override fun fetchBusinessTransactionsFromRepository(period: YearMonth): List<BusinessTransaction> {
                TODO("not implemented")
            }

            override fun fetchBusinessTransactionFromRepository(uuid: String): BusinessTransaction? {
                return transactions[uuid]
            }

        }

        @BeforeAll
        @JvmStatic
        internal fun beforeAll() {
            LOGGER.info("beforeAll called")
        }

        @AfterAll
        @JvmStatic
        internal fun afterAll() {
            LOGGER.info("afterAll called")
        }
    }

    @Test
    fun `when a business transaction is valid in all aspects except that the target does not exist error is thrown`() {

        // === Arrange ===

        val sut = AddBusinessTransactionUseCase(companyProvider,
                addBusinessTransactionInRepo)

        val addSourceBusinessTransactionWithNoExistentTarget = AddBusinessTransactionMessage(
                targetTransactionUUID = UUID.randomUUID().toString(),
                type = BusinessTransactionType.IRD_GST_PAYMENT,
                completedDate = LocalDate.now(),
                amountInCents = 2000,
                gstInCents = 0
        )

        // === Act & Assert ===
        Assertions.assertThrows(BusinessTransactionException::class.java) {
            sut.addBusinessTransaction(addSourceBusinessTransactionWithNoExistentTarget)
        }
    }

    @Test
    fun `can add a transaction, then another with previous as parent and link shows up in the parent when parent fetched`() {

        // === Arrange ===

        val addTargetBusinessTransaction = AddBusinessTransactionMessage(
                type = BusinessTransactionType.INVOICE_PAYMENT,
                completedDate = LocalDate.now(),
                amountInCents = 2000,
                gstInCents = 0
        )

        val sut = AddBusinessTransactionUseCase(companyProvider,
                                                addBusinessTransactionInRepo)

        val fetch = FetchBusinessTransactionsUseCase(fetchBusinessTransactionsFromRepo)

        val targetBusinessTransaction: BusinessTransaction = sut.addBusinessTransaction(addTargetBusinessTransaction)

        val addSourceBusinessTransaction = AddBusinessTransactionMessage(
                // NOTE the link between the transaction below
                targetTransactionUUID = targetBusinessTransaction.uuid,
                type = BusinessTransactionType.IRD_GST_PAYMENT,
                completedDate = LocalDate.now(),
                amountInCents = 2000,
                gstInCents = 0
        )

        val sourceBusinessTransaction: BusinessTransaction = sut.addBusinessTransaction(addSourceBusinessTransaction)

        assertThat(sourceBusinessTransaction).isNotNull

        // === Act ===

        val fetchedTarget: BusinessTransaction? = fetch.fetchBusinessTransaction(targetBusinessTransaction.uuid)

        // === Assert ===

        assertThat(fetchedTarget).isNotNull
        assertThat(fetchedTarget?.sourceTransactions).isNotNull
        assertThat(fetchedTarget?.sourceTransactions?.size).isEqualTo(1)

    }
}