package com.thorgil.cashbook.core.entity

import com.thorgil.cashbook.core.usecase.business.transaction.invariant.BusinessTransactionException
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import java.time.LocalDate
import java.time.Month

class BusinessTransactionTest {

    companion object {
        val companyConfiguration = CompanyConfiguration(entityName = "ABC Limited",
                companyNumber = "123",
                NZBN = "123",
                incorporationDate = LocalDate.now(),
                annualReturnFilingMonth = Month.JUNE,
                gstStatus = GstStatus.REGISTERED)
    }

    @Test
    fun `a completed BusinessTransaction must have a date equal to or before today`() {

        // === Arrange ===
        val tomorrow = LocalDate.now().plusDays(1)

        // === Act & Assert ===
        val exception = Assertions.assertThrows(BusinessTransactionException::class.java) {
            val addBusinessTransWithIncorrectSchedDate = BusinessTransaction(
                    type = BusinessTransactionType.INVOICE_PAYMENT,
                    completedDate = tomorrow,
                    scheduledDate = null,
                    sourceTransactions = setOf<BusinessTransaction>(),
                    companyConfig = companyConfiguration,
                    amountInCents = 2000,
                    gstInCents = 300
            )
        }
        Assertions.assertNotNull(exception)
        Assertions.assertEquals("A completed BusinessTransaction must have a date equal to or before today",
                exception.message)
    }

    @Test
    fun `source completed date after target schedule date is not valid`() {

        // === Arrange ===

        val sourceTransactionCompleted = BusinessTransaction(
                type = BusinessTransactionType.INVOICE_PAYMENT,
                completedDate = LocalDate.now(),
                scheduledDate = null,
                sourceTransactions = setOf<BusinessTransaction>(),
                companyConfig = companyConfiguration,
                amountInCents = 2000,
                gstInCents = 300
        )

        val targetTransactionScheduled = BusinessTransaction(
                type = BusinessTransactionType.INVOICE_PAYMENT,
                completedDate = null,
                scheduledDate = sourceTransactionCompleted.completedDate?.minusDays(1),
                sourceTransactions = setOf<BusinessTransaction>(),
                companyConfig = companyConfiguration,
                amountInCents = 2000,
                gstInCents = 300
        )

        // === Act & Assert ===
        val exception = Assertions.assertThrows(BusinessTransactionException::class.java) {
            targetTransactionScheduled.validateSourceRelativeToThisTarget(sourceTransactionCompleted)
        }
        Assertions.assertNotNull(exception)

    }

    @Test
    fun `source completed date before target schedule date is valid`() {

        // === Arrange ===

        val sourceTransactionCompleted = BusinessTransaction(
                type = BusinessTransactionType.INVOICE_PAYMENT,
                completedDate = LocalDate.now(),
                scheduledDate = null,
                sourceTransactions = setOf<BusinessTransaction>(),
                companyConfig = companyConfiguration,
                amountInCents = 2000,
                gstInCents = 300
        )

        val targetTransactionScheduled = BusinessTransaction(
                type = BusinessTransactionType.INVOICE_PAYMENT,
                completedDate = null,
                scheduledDate = sourceTransactionCompleted.completedDate?.plusDays(1),
                sourceTransactions = setOf<BusinessTransaction>(),
                companyConfig = companyConfiguration,
                amountInCents = 2000,
                gstInCents = 300
        )

        // === Act & Assert ===

        val valid = targetTransactionScheduled.validateSourceRelativeToThisTarget(sourceTransactionCompleted)
        assertThat(valid).isTrue()
    }

    @Test
    fun `source and target scheduled at the same local date is relatively valid`() {

        // === Arrange ===

        val scheduledDate = LocalDate.now()

        val sourceTransactionCompleted = BusinessTransaction(
                type = BusinessTransactionType.INVOICE_PAYMENT,
                completedDate = null,
                scheduledDate = scheduledDate,
                sourceTransactions = setOf<BusinessTransaction>(),
                companyConfig = companyConfiguration,
                amountInCents = 2000,
                gstInCents = 300
        )

        val targetTransactionScheduled = BusinessTransaction(
                type = BusinessTransactionType.INVOICE_PAYMENT,
                completedDate = null,
                scheduledDate = scheduledDate,
                sourceTransactions = setOf<BusinessTransaction>(),
                companyConfig = companyConfiguration,
                amountInCents = 2000,
                gstInCents = 300
        )

        // === Act & Assert ===

        val valid = targetTransactionScheduled.validateSourceRelativeToThisTarget(sourceTransactionCompleted)
        assertThat(valid).isTrue()
    }
}