package com.thorgil.cashbook.core.entity

import com.thorgil.cashbook.core.usecase.business.transaction.invariant.BusinessTransactionErrorCode
import com.thorgil.cashbook.core.usecase.business.transaction.invariant.BusinessTransactionException
import com.thorgil.cashbook.dataproviders.entity.BusinessTransactionEntity
import com.thorgil.cashbook.entrypoints.rest.business.transaction.BusinessTransactionDTO
import java.time.LocalDate
import java.time.LocalDateTime
import java.util.*

/**
 * BusinessTransactions are a record of all payments (expenses) and receipts (income).
 *
 * To be clear a BusinessTransaction always has a movement of money associated with it (and so can be reconciled with
 * one of the CompanyConfiguration's bank accounts) with the exception of BusinessTransactions that have a scheduledDate set and no
 * completedDate. The combination of the former scenario can be used to model future expected BusinessTransactions (this
 * is to help with recording liabilities such as various forms of tax to the IRD).
 *
 * In terms of modelling future transactions, when it comes to certain types of liabilities (e.g. paying over employee
 * income tax to the IRD) it is likely that one future transaction will be linked to many immediately completed
 * BusinessTransactions. This is because one payment to the IRD (in future) would be linked to many individual
 * sourceTransactions.
 *
 * Future BusinessTransactions could also be used to model when the business is expecting to have invoices paid.
 *
 * In terms of recording a BusinessTransactionType initially what matters is whether it is of type EXPENSE (debit) or
 * INCOME (credit). Breaking it down further can be done of course.
 *
 * Note, whether a BusinessTransaction includes GST is based on the associated company configuration (which is a model
 * of whether the company is registered for GST) at the time when the BusinessTransaction has been created.
 *
 * ===========================================
 * === Start of Domain-Driven Design Notes ===
 * ===========================================
 *
 * - BusinessTransaction is a ROOT ENTITY but it is not an AGGREGATE
 *
 * - In terms of identity, how does this model define what it means to be the same thing and how is this meaning
 * superimposed because it is useful?
 *
 * The answer depends on the type of the transaction.
 *
 * If it is a scheduled transaction (one without a completed date ...
 *
 * NOTE: in terms of the lifecycle of this domain object, we maintain integrity throughout the lifecycle by
 * using Kotlin's initializer blocks for validation (a RuntimeException will be thrown if this is not the case).
 * This means that our business validation rules are not littered throughout the codebase in say use-case classes
 * (this can happen quite easily and may be hard to spot). It also means that whenever you are dealing with a domain
 * entity you know that it is valid. We also use immutable instance variables as a rule, and so our domain objects
 * are thread-safe as a result.
 *
 * The reason why BusinessTransaction is not an AGGREGATE is because some rules for aggregates do not hold ????
 *
 * - [TRUE] The root Entity has global identity and is ultimately responsible for checking invariants
 * - [FALSE] Root Entities have global identity. Entities inside the boundary have local identity, unique
 *              only within the Aggregate. ?????
 * - [TRUE] Nothing outside the Aggregate boundary can hold a reference to anything inside, except to the root Entity.
 *          The root Entity can hand references to the internal Entities to other objects, but they can only use them
 *          transiently (within a single method or block).
 * - [TRUE] Only Aggregate Roots can be obtained directly with database queries.  Everything else must be done through
 *          traversal.
 * - [TRUE] Objects within the Aggregate can hold references to other Aggregate roots.
 * - [FALSE] A delete operation must remove everything within the Aggregate boundary all at once.?????
 * - [TRUE] When a change to any object within the Aggregate boundary is committed, all invariants of the whole
 *          Aggregate must be satisfied.
 *
 * @param uuid unique identifier for this transaction
 * @param createdTimestamp
 * @param lastUpdateTimestamp
 * @param scheduledDate if this is set and completed is not set then this is a future (scheduled) transaction
 * @param completedDate if this is set the transaction occurred (reconciliation can occur) and no further action
 *                      is expected
 * @param type
 * @param amountInCents this is always exclusive of GST
 * @param gstInCents
 * @param sourceTransactions transactions that are linked to this one in a source relationship such as multiple invoices
 *                          with GST paid being linked to a target GST payment business transaction
 * @param evidenceLink
 * @param businessTransactionIssue an issue picked up by the system
 * @param businessTransactionIssueDetail free form text with further detail of the issue
 *
 * @throws BusinessTransactionException if a wide range of invariants are not met
 */
open class BusinessTransaction constructor(val uuid: String = UUID.randomUUID().toString(),
                                           val createdTimestamp: LocalDateTime? = null,
                                           val lastUpdateTimestamp: LocalDateTime? = null,
                                           val scheduledDate: LocalDate?,
                                           val completedDate: LocalDate?,
                                           val type: BusinessTransactionType,
                                           val amountInCents: Int,
                                           val gstInCents: Int,
                                           val sourceTransactions: Set<BusinessTransaction>,
                                           val evidenceLink: String? = null,
                                           val companyConfig: CompanyConfiguration) {

    init {

        if (scheduledDate == null && completedDate == null) {
            throw BusinessTransactionException(BusinessTransactionErrorCode.BOTH_SCHEDULED_COMPLETED_DATE_NULL,
                    BOTH_SCHEDULED_COMPLETED_DATE_NULL)
        }
        if (completedDate != null && scheduledDate != null) {
            throw BusinessTransactionException(BusinessTransactionErrorCode.CANNOT_BOTH_BE_SCHEDULED_AND_COMPLETED,
                    "A completed BusinessTransaction cannot also be scheduled")
        }

        if (scheduledDate == null &&
                completedDate != null &&
                LocalDate.now().isBefore(completedDate)) {
            throw BusinessTransactionException(BusinessTransactionErrorCode.COMPLETED_DATE_MUST_BE_BEFORE_OR_EQUAL_TO_TODAY,
                    "A completed BusinessTransaction must have a date equal to or before today")
        }

        if (amountInCents < 0) {
            throw BusinessTransactionException(BusinessTransactionErrorCode.AMOUNT_CANNOT_BE_NEGATIVE,
                    "BusinessTransaction amount must be greater than or equal to 0 when supplied")
        }

        if (gstInCents < 0) {
            throw BusinessTransactionException(BusinessTransactionErrorCode.GST_AMOUNT_CANNOT_BE_NEGATIVE,
                    "BusinessTransaction GST amount must be greater or equal to than 0 when supplied")
        }

        for (sourceTransaction in sourceTransactions) {

            validateSourceRelativeToThisTarget(sourceTransaction)
        }

    }

    val businessTransactionIssue: BusinessTransactionIssue
    val businessTransactionIssueDetail: String?

    init {

        // this must be an issue and not an invariant because if we reconstitute this entity from persistent
        // storage and the scheduled date if PAST the current date (say because the user has not acted on the
        // scheduled trans) then they'll never be able to view the record
        if (completedDate == null &&
                scheduledDate != null &&
                LocalDate.now().isAfter(scheduledDate)) {

            businessTransactionIssue = BusinessTransactionIssue.SCHEDULED_DATE_MUST_BE_FOR_A_FUTURE_DATE
            businessTransactionIssueDetail = "A scheduled BusinessTransaction must be scheduled for a future date"
            
        } else if (companyConfig.gstStatus == GstStatus.REGISTERED &&
                type == BusinessTransactionType.INVOICE_PAYMENT &&
                amountInCents != 0) {

            val calculatedGstInCents = (amountInCents * CompanyConfiguration.GST_PERCENTAGE).toInt()

            if (calculatedGstInCents != gstInCents) {
                businessTransactionIssue = BusinessTransactionIssue.INVOICE_PAYMENT_WITH_INCORRECT_GST
                businessTransactionIssueDetail = "GST of $calculatedGstInCents expected"
            } else {
                businessTransactionIssue = BusinessTransactionIssue.NONE
                businessTransactionIssueDetail = null
            }

        } else {

            businessTransactionIssue = BusinessTransactionIssue.NONE
            businessTransactionIssueDetail = null

        }

    }

    companion object {
        val BOTH_SCHEDULED_COMPLETED_DATE_NULL = "Both scheduledDate and completedDate cannot be null"
    }

    /**
     * Here we are following Eric Evans' advice, in Domain-Driven-Design, Where Does Invariant Logic Go? pg 143
     * "Think twice before removing the rules applying to an object outside that object. The FACTORY can
     * delegate invariant checking to the product, and this if often best"
     */
    fun validateSourceRelativeToThisTarget(source: BusinessTransaction): Boolean {

        val target: BusinessTransaction = this

        if (target.uuid == source.uuid) {
            throw BusinessTransactionException(BusinessTransactionErrorCode.CHILD_SAME_AS_THIS_ENTITY_LOGICAL_ERROR,
                    "source <$source.uuid> and target <$target.uuid> are the same")
        }
        if (source.completedDate != null && target.completedDate != null) {
            if (source.completedDate.isAfter(target.completedDate)) {
                throw BusinessTransactionException(
                        BusinessTransactionErrorCode.SOURCE_COMPLETED_DATE_AFTER_TARGET_COMPLETED_DATE,
                        "source <$source.uuid> must have a completed date equal to or before target completed date <$target.uuid>")
            }
        } else if (source.completedDate != null &&
                target.completedDate == null &&
                target.scheduledDate != null &&
                source.completedDate.isAfter(target.scheduledDate)) {

                throw BusinessTransactionException(
                        BusinessTransactionErrorCode.SOURCE_COMPLETED_DATE_AFTER_TARGET_SCHEDULED_DATE,
                        "source <$source.uuid> must have a completed date equal to or before target scheduled date <$target.uuid>")

        }
        return true;

    }

    /**
     * A VALUE OBJECT FACTORY
     */
    fun createCopyWithAddedSourceTransaction(additionalSourceTrans: BusinessTransaction): BusinessTransaction {

        val set = mutableSetOf<BusinessTransaction>()
        set.addAll(this.sourceTransactions)

        val elementAddedAndSoNotDuplicate = set.add(additionalSourceTrans)
        if (!elementAddedAndSoNotDuplicate) {
            throw BusinessTransactionException(BusinessTransactionErrorCode.SOURCE_AND_TARGET_ALREADY_ASSOCIATED,
                    "attempt at adding duplicate source transaction: $additionalSourceTrans")
        }

        return BusinessTransaction(uuid = this.uuid,
                                    createdTimestamp = this.createdTimestamp,
                                    lastUpdateTimestamp = this.lastUpdateTimestamp,
                                    scheduledDate = this.scheduledDate,
                                    completedDate = this.completedDate,
                                    type = this.type,
                                    amountInCents = this.amountInCents,
                                    gstInCents = this.gstInCents,
                                    sourceTransactions = set.toHashSet(),
                                    evidenceLink = this.evidenceLink,
                                    companyConfig = this.companyConfig)
    }

    fun toBusinessTransactionDTO(): BusinessTransactionDTO {
        val childTransactions = mutableListOf<BusinessTransactionDTO>()
        if (this.sourceTransactions.isNotEmpty()) {
            for (childTransaction in this.sourceTransactions) {
                childTransactions.add(childTransaction.toBusinessTransactionDTO())
            }
        }

        return BusinessTransactionDTO(
                uuid = this.uuid,
                createdTimestamp = this.createdTimestamp,
                lastUpdateTimestamp = this.lastUpdateTimestamp,
                scheduledDate = this.scheduledDate,
                completedDate = this.completedDate,
                type = this.type.toString(),
                amountInCents = this.amountInCents,
                gstInCents = this.gstInCents,
                sourceTransactions = childTransactions,
                evidenceLink = this.evidenceLink,
                businessTransactionIssue = this.businessTransactionIssue.toString(),
                businessTransactionIssueDetail = this.businessTransactionIssueDetail
        )
    }

    fun toBusinessTransactionEntity(): BusinessTransactionEntity {
        val childTransactions = mutableListOf<BusinessTransactionEntity>()
        if (this.sourceTransactions.isNotEmpty()) {
            for (childTransaction in this.sourceTransactions) {
                childTransactions.add(childTransaction.toBusinessTransactionEntity())
            }
        }
        return BusinessTransactionEntity(
                uuid = this.uuid,
                type = this.type.toString(),
                amountInCents = this.amountInCents,
                gstInCents = this.gstInCents,
                sources = childTransactions,
                scheduledDate = this.scheduledDate,
                completedDate = this.completedDate,
                evidenceLink = this.evidenceLink
        )
    }

    override fun toString(): String {
        return "BusinessTransaction(uuid='$uuid', createdTimestamp=$createdTimestamp, " +
                "lastUpdateTimestamp=$lastUpdateTimestamp, " +
                "scheduledDate=$scheduledDate, " +
                "completedDate=$completedDate, " +
                "type=$type, " +
                "amountInCents=$amountInCents, " +
                "gstInCents=$gstInCents, " +
                "sourceTransactions=$sourceTransactions, " +
                "evidenceLink=$evidenceLink, " +
                "businessTransactionIssue=$businessTransactionIssue, " +
                "businessTransactionIssueDetail=$businessTransactionIssueDetail)"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) {
            return true
        }
        if (javaClass != other?.javaClass) {
            return false
        }
        other as BusinessTransaction
        if (uuid != other.uuid) {
            return false
        }
        return true
    }

    override fun hashCode(): Int {
        return uuid.hashCode()
    }
}

/**
 * Note this enum holds a hierarchical data structure.
 *
 * It would be handy to explicitly model Zero-rated supplies.
 *
 * See: https://dzone.com/articles/enum-tricks-hierarchical-data
 */
enum class BusinessTransactionType {
    EXPENSE {
        override fun parent() = null
    },
    INCOME {
        override fun parent() = null
    },
    BANK_FEE_PAYMENT {
        override fun parent() = EXPENSE
    },
    SALARY_PAYMENT {
        override fun parent() = EXPENSE
    },
    SUPPLIER_INVOICE_PAYMENT {
        override fun parent() = EXPENSE
    },
    IRD_SALARY_OVERHEAD_PAYMENT {
        override fun parent() = EXPENSE
    },
    IRD_GST_PAYMENT {
        override fun parent() = EXPENSE
    },
    BANK_WITHHOLDING_TAX {
        override fun parent() = EXPENSE
    },
    INVOICE_PAYMENT {
        override fun parent() = INCOME
    },
    CREDIT_INTEREST_PAYMENT {
        override fun parent() = INCOME
    },
    OWNER_CONTRIBUTION_PAYMENT {
        override fun parent() = INCOME
    };
    abstract fun parent(): BusinessTransactionType?
}

enum class BusinessTransactionIssue {
    NONE {
        override fun parent() = null
    },
    GST {
        override fun parent() = null
    },
    INVOICE_PAYMENT_WITH_INCORRECT_GST {
        override fun parent() = GST
    },
    SCHEDULED_DATE_MUST_BE_FOR_A_FUTURE_DATE {
        override fun parent() = null
    };

    abstract fun parent(): BusinessTransactionIssue?
}