package com.thorgil.cashbook.core.usecase.business.transaction

import com.thorgil.cashbook.core.entity.BusinessTransaction
import java.time.YearMonth

interface FetchBusinessTransactionsFromRepository {

    /**
     * Fetch all BusinessTransactions with completedDate or scheduledDate within the cited
     * month.
     *
     * Must execute in the context of a transaction with read semantics.
     */
    fun fetchBusinessTransactionsFromRepository(period: YearMonth): List<BusinessTransaction>

    /**
     * Fetch a specific BusinessTransaction.
     *
     * Must execute in the context of a transaction with read semantics.
     */
    fun fetchBusinessTransactionFromRepository(uuid: String): BusinessTransaction?
}