package com.thorgil.cashbook.core.usecase.business.transaction

import com.thorgil.cashbook.core.entity.BusinessTransaction
import com.thorgil.cashbook.dataproviders.exception.BusinessTransactionPersistenceException

interface LinkBusinessTransactionsInRepository {

    /**
     * Link the specified source transactions with the provided target transactions in the repository.
     *
     * The exact semantics of the association depends on the type of the target transaction. The cited semantics are not
     * the concern of this method however.
     *
     * Preconditions:
     *
     * 1. All specified transactions already exist in the system.
     *
     * @param sourceUuid
     * @param targetUid
     * @throws BusinessTransactionPersistenceException when an unrecoverable persistence invariant
     *         occurs OR when precondition 1 does not hold
     * @return the target business transaction after the link has been established
     */
    @Throws(BusinessTransactionPersistenceException::class)
    fun linkBusinessTransactionsInRepository(sourceUuid: String, targetUid: String): BusinessTransaction?
}