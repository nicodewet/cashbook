package com.thorgil.cashbook.core.usecase.business.transaction

import com.thorgil.cashbook.core.entity.BusinessTransaction
import com.thorgil.cashbook.dataproviders.exception.BusinessTransactionPersistenceException

interface AddBusinessTransactionInRepository {

    /**
     * This method must execute in the context of a transaction with read-write semantics.
     *
     * @throws BusinessTransactionPersistenceException when an unrecoverable persistence invariant
     *         occurs OR when the targetTransactionUuid does not exist in the repository
     */
    @Throws(BusinessTransactionPersistenceException::class)
    fun addBusinessTransactionInRepository(businessTransaction: BusinessTransaction, targetTransactionUuid: String?)

}