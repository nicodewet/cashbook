package com.thorgil.cashbook.core.usecase.business.transaction.invariant

/**
 * The code describes what went wrong i.e. the invariant condition in terms of state. Also the context of the invariant
 * codes is already described by the class name so is not repeated in the invariant codes (i.e. no mention of the LINK
 * verb or BUSINESS_TRANSACTION).
 *
 * @param errorCode the invariant in an encoded form
 * @param message the invariant message in human readable form, can be displayed to users for action or logged for
 *                posterity
 */
class BusinessTransactionException(val errorCode: BusinessTransactionErrorCode, message: String)
    : RuntimeException(message) {

}