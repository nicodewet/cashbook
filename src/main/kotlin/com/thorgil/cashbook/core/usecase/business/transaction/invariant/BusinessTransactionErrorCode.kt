package com.thorgil.cashbook.core.usecase.business.transaction.invariant

/**
 * The code describes what went wrong i.e. the invariant condition in terms of state. Also the context of the invariant
 * codes is already described by the class name so is not repeated in the invariant codes (i.e. no mention of the LINK
 * verb or BUSINESS_TRANSACTION).
 */
enum class BusinessTransactionErrorCode {
    /**
     *
     */
    SOURCE_DOES_NOT_EXIST,
    /**
     *
     */
    TARGET_DOES_NOT_EXIST,
    /**
     *
     */
    SOURCE_AND_TARGET_ALREADY_ASSOCIATED,
    /**
     *
     */
    SOURCE_COMPLETED_DATE_AFTER_TARGET_COMPLETED_DATE,
    /**
     *
     */
    SOURCE_COMPLETED_DATE_AFTER_TARGET_SCHEDULED_DATE,
    /**
     *
     */
    BOTH_SCHEDULED_COMPLETED_DATE_NULL,
    /**
     *
     */
    CANNOT_BOTH_BE_SCHEDULED_AND_COMPLETED,
    /**
     *
     */
    CHILD_SAME_AS_THIS_ENTITY_LOGICAL_ERROR,
    /**
     *
     */
    COMPLETED_DATE_MUST_BE_BEFORE_OR_EQUAL_TO_TODAY,
    /**
     *
     */
    AMOUNT_CANNOT_BE_NEGATIVE,
    /**
     *
     */
    GST_AMOUNT_CANNOT_BE_NEGATIVE,
    /**
     *
     */
    //PERSISTENCE_ERROR
}