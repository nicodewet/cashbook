package com.thorgil.cashbook.core.usecase.business.transaction

import com.thorgil.cashbook.core.entity.*
import com.thorgil.cashbook.core.usecase.business.transaction.invariant.BusinessTransactionException
import com.thorgil.cashbook.core.usecase.company.GetCompany

interface AddBusinessTransaction {

    /**
     * Add a cashbook transaction to the system.
     *
     * @param businessTransaction may be a source (target specified) or stand-alone business transaction (no target specified)
     * @throws BusinessTransactionException indicates a business rule validation invariant
     * @return when adding a child transaction the target transaction should be returned in the response
     */
    fun addBusinessTransaction(businessTransaction: AddBusinessTransactionMessage): BusinessTransaction

}

class AddBusinessTransactionUseCase(private val companyProvider: GetCompany,
                                    private val addBusinessTransactionInRepo: AddBusinessTransactionInRepository): AddBusinessTransaction {

    override fun addBusinessTransaction(businessTransaction: AddBusinessTransactionMessage): BusinessTransaction {

        val theBusinessTransaction
                = BusinessTransaction(type = businessTransaction.type,
                                    scheduledDate = businessTransaction.scheduledDate,
                                    completedDate = businessTransaction.completedDate,
                                    amountInCents = businessTransaction.amountInCents,
                                    gstInCents = businessTransaction.gstInCents,
                                    evidenceLink = businessTransaction.evidenceLink,
                                    // the source transactions are irrelevant if this use case
                                    sourceTransactions = hashSetOf(),
                                    companyConfig = companyProvider.getCompany())

        // remember that the repo will throw an exception of the target trans does not exist
        addBusinessTransactionInRepo.addBusinessTransactionInRepository(theBusinessTransaction, businessTransaction.targetTransactionUUID)

        return theBusinessTransaction

    }

}