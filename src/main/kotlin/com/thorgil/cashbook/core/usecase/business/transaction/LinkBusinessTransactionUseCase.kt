package com.thorgil.cashbook.core.usecase.business.transaction

import com.thorgil.cashbook.core.entity.BusinessTransaction
import com.thorgil.cashbook.core.usecase.business.transaction.invariant.BusinessTransactionErrorCode
import com.thorgil.cashbook.core.usecase.business.transaction.invariant.BusinessTransactionException

interface LinkBusinessTransactions {

    /**
     * Link the specified source transaction with the specified target.
     *
     * The exact semantics of the association depends on the type of the target transaction.
     *
     * Preconditions:
     *
     * 1. All specified transactions already exist in the system.
     * 2. The target transaction uuid cannot be the same as the source uuids.
     * 3. The source transactions must have a completed date that is less than or equal to the target transactions'
     * completed date or scheduled date. If the target transaction has a completed date then only this date gets
     * considered because the schedule date is redundant (ignored).
     *
     * @param sourceUuid
     * @param targetUid
     * @return the updated target business transaction, we expect to see the new source transaction present
     * @throws LinkBusinessTransactionException
     * @throws IllegalArgumentException if any uuid is blank OR a the targetUUid is in the list of source uuids
     */
    fun linkBusinessTransactions(sourceUuid: String, targetUid: String): BusinessTransaction?
}
class LinkBusinessTransactionUseCase(private val linkBusinessTransactionsInRepo: LinkBusinessTransactionsInRepository,
                                     private val fetchBusinessTransactions: FetchBusinessTransactionsFromRepository): LinkBusinessTransactions {

    override fun linkBusinessTransactions(sourceUuid: String, targetUid: String): BusinessTransaction? {

        /**
         * Basic validation. These are programmer errors, hence IllegalArgumentException in all cases.
         */
        if (sourceUuid.isBlank()) {
            throw IllegalArgumentException("source uuid cannot be blank")
        }
        if (targetUid.isBlank()) {
            throw IllegalArgumentException("target uuid cannot be blank")
        }
        if (sourceUuid == targetUid) {
            throw IllegalArgumentException("source and target uuids cannot match")
        }

        /**
         * At this stage we can enforce business rules, and so we fetch the cited
         * transactions from the repository
         */
        val target = fetchBusinessTransactions.fetchBusinessTransactionFromRepository(targetUid)
        if (target != null) {
            for (source in target.sourceTransactions) {
                if (sourceUuid == source.uuid) {
                    throw BusinessTransactionException(
                            BusinessTransactionErrorCode.SOURCE_AND_TARGET_ALREADY_ASSOCIATED,
                            "source <$sourceUuid> already associated with <$targetUid>")
                }
            }
        } else {
            throw BusinessTransactionException(
                    BusinessTransactionErrorCode.TARGET_DOES_NOT_EXIST,
                    "target <$targetUid> does not exist")
        }
        val source = fetchBusinessTransactions.fetchBusinessTransactionFromRepository(sourceUuid)
        if (source == null) {
            throw BusinessTransactionException(
                    BusinessTransactionErrorCode.SOURCE_DOES_NOT_EXIST,
                    "source <$sourceUuid> does not exist")
        } else {

            // Here we delegate invariant checking to the BusinessTransaction
            target.validateSourceRelativeToThisTarget(source)

        }

        /**
         * ..and finally establish the link in the repo
         */
        //try {
            return linkBusinessTransactionsInRepo.linkBusinessTransactionsInRepository(sourceUuid, targetUid)
        //} catch (e: BusinessTransactionPersistenceException) {
        //    throw BusinessTransactionException(BusinessTransactionErrorCode.PERSISTENCE_ERROR, e)
        //}

    }

}