package com.thorgil.cashbook.entrypoints.rest.business.transaction

data class LinkBusinessTransactionPostBody(
                                            val sourceTransactionUuid: String? = null,
                                            val targetTransactionUuid: String? = null
                                          )