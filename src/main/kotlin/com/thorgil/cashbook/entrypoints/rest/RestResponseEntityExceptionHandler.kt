package com.thorgil.cashbook.entrypoints.rest

import com.thorgil.cashbook.core.entity.BusinessTransaction
import com.thorgil.cashbook.core.usecase.business.transaction.invariant.BusinessTransactionException
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.MessageSource
import org.springframework.http.HttpStatus
import org.springframework.validation.FieldError
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.ResponseStatus
import javax.validation.ConstraintViolationException

@ControllerAdvice
class RestResponseEntityExceptionHandler @Autowired constructor(var messageSource: MessageSource) {

    private val log = LoggerFactory.getLogger(RestResponseEntityExceptionHandler::class.java)

    @ExceptionHandler(MethodArgumentNotValidException::class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    fun processValidationError(ex: MethodArgumentNotValidException): ValidationErrorDTO {
        val result = ex.bindingResult
        val fieldErrors = result.fieldErrors

        log.info("Processing ${fieldErrors.size} field invariant(s)")

        val validationErrorDTO = processFieldErrors(fieldErrors)

        log.info(validationErrorDTO.toString())

        return validationErrorDTO
    }

    @ExceptionHandler(BusinessTransactionException::class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    fun processBusinessValidationError(ex: BusinessTransactionException): ValidationErrorDTO {

        val  validationErrorDTO = ValidationErrorDTO()

        if (ex.message == BusinessTransaction.BOTH_SCHEDULED_COMPLETED_DATE_NULL) {
            // remember that this is in effect a transformation, it's not a given that field names in the
            // RESTful layer will be the same as in the com.thorgil.cashbook.core layer
            validationErrorDTO.addFieldError("scheduledDate",
                    BusinessTransaction.BOTH_SCHEDULED_COMPLETED_DATE_NULL)
            validationErrorDTO.addFieldError("completedDate",
                    BusinessTransaction.BOTH_SCHEDULED_COMPLETED_DATE_NULL)
        } else {
            validationErrorDTO.addFieldError("", ex.message.orEmpty())
        }

        return validationErrorDTO
    }

    @ExceptionHandler(ConstraintViolationException::class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    fun processRequestParameterValidationError(ex: ConstraintViolationException): ValidationErrorDTO {
        val constraintViolations = ex.constraintViolations

        log.info("Processing ${constraintViolations.size} constraint violation(s)")

        val  validationErrorDTO = ValidationErrorDTO()

        for (constraintViolation in constraintViolations) {
            validationErrorDTO.addFieldError(constraintViolation.propertyPath.toString(), constraintViolation.invalidValue.toString() + " " + constraintViolation.message)
        }

        return validationErrorDTO
    }

    private fun processFieldErrors(fieldErrors: List<FieldError>): ValidationErrorDTO {
        val dto = ValidationErrorDTO()

        for (fieldError in fieldErrors) {

            val fieldErrorCodes = fieldError.codes
            val fieldErrorCodeMessage = fieldErrorCodes!![0]

            dto.addFieldError(fieldError.field, fieldError.defaultMessage?: fieldErrorCodeMessage)
        }

        return dto
    }
}