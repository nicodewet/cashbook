package com.thorgil.cashbook.dataproviders

import java.time.LocalDateTime
import java.time.LocalTime
import java.time.YearMonth

class DateUtil {
    companion object {
        fun getStartTimeStamp(yearMonth: YearMonth): LocalDateTime {
            return yearMonth.atDay(1).atStartOfDay()
        }
        fun getEndTimeStamp(yearMonth: YearMonth): LocalDateTime {
            return yearMonth.atEndOfMonth().atTime(LocalTime.MAX)
        }
    }
}