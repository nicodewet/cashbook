package com.thorgil.cashbook.dataproviders

import com.thorgil.cashbook.core.entity.BusinessTransaction
import com.thorgil.cashbook.core.entity.BusinessTransactionType
import com.thorgil.cashbook.core.usecase.business.transaction.AddBusinessTransactionInRepository
import com.thorgil.cashbook.dataproviders.exception.BusinessTransactionPersistenceException
import com.thorgil.cashbook.core.usecase.business.transaction.FetchBusinessTransactionsFromRepository
import com.thorgil.cashbook.core.usecase.business.transaction.LinkBusinessTransactionsInRepository
import com.thorgil.cashbook.core.usecase.business.transaction.invariant.BusinessTransactionErrorCode
import com.thorgil.cashbook.core.usecase.business.transaction.invariant.BusinessTransactionException
import com.thorgil.cashbook.core.usecase.company.GetCompany
import com.thorgil.cashbook.dataproviders.entity.BusinessTransactionEntity
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.time.YearMonth
import javax.transaction.Transactional

/**
 * See the "Using a facade to define transactions for multiple repository calls" example in
 * "5.7. Transactionality" in https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#transactions
 *
 * Particularly pertinent excerpts: "By default, CRUD methods on repository instances are transactional. Another way to
 * alter transactional behaviour is to use a facade or service implementation that (typically) covers more than one
 * repository. Its purpose is to define transactional boundaries for non-CRUD operations."
 *
 * NOTE: in terms of consistency, we need to decide where to do the transformations to com.thorgil.cashbook.core layer entities (DDD
 * terminology, our choice is here i.e. THIS IS THE ONE PLACE where we'll transform from DB entity to com.thorgil.cashbook.core entity),
 * but this is just logical given our layered code structure.
 */
@Service
class BusinessTransactionDataProvider
    : AddBusinessTransactionInRepository, FetchBusinessTransactionsFromRepository, LinkBusinessTransactionsInRepository {

    companion object {
        val TARGET_OBJECT_DOES_NOT_EXIST_MESSAGE = "Target business transaction does not exist"
        val SOURCE_OBJECT_DOES_NOT_EXIST = "Source business transaction does not exist"
    }

    @Autowired
    lateinit var businessTransactionRepo: BusinessTransactionEntityRepository

    @Autowired
    lateinit var companyProvider: GetCompany

    override fun linkBusinessTransactionsInRepository(sourceUuid: String, targetUid: String): BusinessTransaction {

        val targetEntity = businessTransactionRepo.findById(targetUid)
        if (!targetEntity.isPresent) {
            throw BusinessTransactionException(BusinessTransactionErrorCode.TARGET_DOES_NOT_EXIST,
                    TARGET_OBJECT_DOES_NOT_EXIST_MESSAGE + " <$targetUid>")
        }

        val sourceEntity = businessTransactionRepo.findById(sourceUuid)
        if (!sourceEntity.isPresent) {
            throw BusinessTransactionException(BusinessTransactionErrorCode.SOURCE_DOES_NOT_EXIST,
                    SOURCE_OBJECT_DOES_NOT_EXIST + " <$sourceUuid>")
        } else {
            sourceEntity.get().addTarget(targetEntity.get())
            businessTransactionRepo.save(sourceEntity.get())
        }

        val repoEntity = businessTransactionRepo.findById(targetUid).get()

        return fromDBEntityToCoreEntity(repoEntity)

    }

    override fun addBusinessTransactionInRepository(businessTransaction: BusinessTransaction, targetTransactionUuid: String?) {
        System.out.println("PERSIST: " + businessTransaction.toString())
        var parentEntity: BusinessTransactionEntity? = null
        if (targetTransactionUuid != null) {
            val optParentEntity = businessTransactionRepo.findById(targetTransactionUuid)
            if (optParentEntity.isPresent) {
                parentEntity = optParentEntity.get()
            } else {
                throw BusinessTransactionException(BusinessTransactionErrorCode.TARGET_DOES_NOT_EXIST,
                        "$TARGET_OBJECT_DOES_NOT_EXIST_MESSAGE <$targetTransactionUuid>.")
            }
        }
        val entity: BusinessTransactionEntity = businessTransaction.toBusinessTransactionEntity()
        if (parentEntity != null) {
            entity.target = parentEntity
        }

        // in time it may be useful to echo the entity that was saved back to the caller
        businessTransactionRepo.save(entity)
    }

    @Transactional
    override fun fetchBusinessTransactionsFromRepository(period: YearMonth): List<BusinessTransaction> {

        val transactions: MutableMap<String, BusinessTransactionEntity> = mutableMapOf()

        val completedTransactions = businessTransactionRepo.findByCompletedDateBetween(
                                                            DateUtil.getStartTimeStamp(period).toLocalDate(),
                                                            DateUtil.getEndTimeStamp(period).toLocalDate())
        if (completedTransactions != null) {
            for (entity in completedTransactions) {
                transactions.putIfAbsent(entity.uuid, entity)
            }
        }

        val scheduledTransactions = businessTransactionRepo.findByScheduledDateBetween(
                DateUtil.getStartTimeStamp(period).toLocalDate(),
                DateUtil.getEndTimeStamp(period).toLocalDate())

        if (scheduledTransactions != null) {
            for (entity in scheduledTransactions) {
                transactions.putIfAbsent(entity.uuid, entity)
            }
        }

        val transactionList: List<BusinessTransactionEntity> = transactions.values.toList()

        return transformToDtoList(transactionList)
    }

    fun transformToDtoList(dbos: List<BusinessTransactionEntity>?): List<BusinessTransaction> {

        if (dbos != null && dbos.size >= 0) {

            val dtoList = mutableListOf<BusinessTransaction>()

            for (entity in dbos) {

                dtoList.add( fromDBEntityToCoreEntity(entity) )

            }

            return dtoList

        } else {

            return emptyList()

        }

    }

    override fun fetchBusinessTransactionFromRepository(uuid: String): BusinessTransaction? {

        val optEntity = businessTransactionRepo.findById(uuid)

        if (optEntity.isPresent) {

            val entity = optEntity.get()
            return fromDBEntityToCoreEntity(entity)

        } else {

            return null

        }

    }

    fun fromDBEntityToCoreEntity(dbEntity: BusinessTransactionEntity): BusinessTransaction {
        val mutableTransSet = mutableSetOf<BusinessTransaction>()

        if (!dbEntity.sources.isEmpty()) {

            for (child in dbEntity.sources) {
                val theElementHasBeenAdded = mutableTransSet.add( fromDBEntityToCoreEntity(child) )
                if (!theElementHasBeenAdded) {
                    throw BusinessTransactionPersistenceException("Data corruption, duplicate source transaction: ${child.toString()}")
                }
            }
        }

        val trans = BusinessTransaction(
                uuid = dbEntity.uuid,
                createdTimestamp = dbEntity.createdAt,
                lastUpdateTimestamp = dbEntity.updatedAt,
                scheduledDate = dbEntity.scheduledDate,
                completedDate = dbEntity.completedDate,
                type = BusinessTransactionType.valueOf(dbEntity.type),
                amountInCents = dbEntity.amountInCents,
                gstInCents = dbEntity.gstInCents,
                evidenceLink = dbEntity.evidenceLink,
                sourceTransactions = mutableTransSet.toHashSet(),
                companyConfig = companyProvider.getCompany()
        )


        return trans
    }

}