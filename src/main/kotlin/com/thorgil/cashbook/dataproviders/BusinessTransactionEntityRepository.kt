package com.thorgil.cashbook.dataproviders

import com.thorgil.cashbook.dataproviders.entity.BusinessTransactionEntity
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import java.time.LocalDate
import java.time.LocalDateTime

/**
 * https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#repositories.query-methods.query-creation
 */
interface BusinessTransactionEntityRepository: CrudRepository<BusinessTransactionEntity, String> {

    fun findByCompletedDateOrScheduledDate(completedDate: LocalDate?, scheduledDate: LocalDate?): List<BusinessTransactionEntity>?

    fun findByCreatedAtBetween(begin: LocalDateTime, end: LocalDateTime): List<BusinessTransactionEntity>?

    fun findByCompletedDateBetween(begin: LocalDate, end: LocalDate): List<BusinessTransactionEntity>?

    fun findByScheduledDateBetween(begin: LocalDate, end: LocalDate): List<BusinessTransactionEntity>?

}