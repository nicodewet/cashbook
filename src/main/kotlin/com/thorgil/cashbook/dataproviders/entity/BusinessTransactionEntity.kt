package com.thorgil.cashbook.dataproviders.entity

import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.UpdateTimestamp
import java.time.LocalDate
import java.time.LocalDateTime
import javax.persistence.*

@Entity
class BusinessTransactionEntity(@Id
                                val uuid: String,
                                var type: String,
                                var amountInCents: Int,
                                var gstInCents: Int,
                                @Column(nullable = true)
                                var scheduledDate: LocalDate? = null,

                                @Column(nullable = true)
                                var completedDate: LocalDate? = null,

                                @Column(nullable = true)
                                var evidenceLink: String? = null,

                                @ManyToOne
                                @JoinColumn(nullable=true)
                                var target: BusinessTransactionEntity? = null,

                                @OneToMany(mappedBy="target", cascade = [CascadeType.ALL], fetch = FetchType.EAGER)
                                val sources: MutableList<BusinessTransactionEntity> = mutableListOf())
    {
        @CreationTimestamp
        @field:CreationTimestamp
        var createdAt: LocalDateTime = LocalDateTime.now()

        @UpdateTimestamp
        @field:UpdateTimestamp
        lateinit var updatedAt: LocalDateTime

        fun addTarget(target: BusinessTransactionEntity) {
            this.target = target
            target.sources.add(this)
        }

        override fun toString(): String {
            return "BusinessTransactionEntity(uuid='$uuid', " +
                    "type='$type', amountInCents=$amountInCents, " +
                    "gstInCents=$gstInCents, " +
                    "scheduledDate=$scheduledDate, " +
                    "completedDate=$completedDate, " +
                    "evidenceLink=$evidenceLink, " +
                    "target=$target, " +
                    "sources=$sources, " +
                    "createdAt=$createdAt, " +
                    "updatedAt=$updatedAt)"
        }

    }